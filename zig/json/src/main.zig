const std = @import("std");

pub fn main() anyerror!void {
    var GPA = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = GPA.deinit();
    var allor = GPA.allocator();

    var args = std.process.ArgIterator.init();
    // Get argv[0].
    const exe = try args.next(allor).?;
    // Get argv[1].
    const filename = try args.next(allor) orelse {
        std.debug.print("Usage: {s} JSON-FILE\n", .{exe});
        std.process.exit(1);
    };
    // Open the file.
    const file = std.fs.cwd().openFile(filename, .{}) catch |err| {
        std.debug.print("input file: {s}: {s}\n", .{ filename, @errorName(err) });
        std.process.exit(1);
    };
    defer file.close();
    // Read the file.
    // FIXME: @maxValue
    const data = try file.readToEndAlloc(allor, 1 << 60);
    // Parse.
    var parser = std.json.Parser.init(allor, false);
    defer parser.deinit();
    const jso = try parser.parse(data);
    defer jso.deinit();

    var iter = jso.root.Object.iterator();
    while (iter.next()) |pair| {
        std.debug.print("{s} → {}\n", .{ pair.key_ptr.*, pair.value_ptr });
    }
    jso.root.dump();

    std.process.exit(0);
}

test "basic test" {
    try std.testing.expectEqual(10, 3 + 7);
}
