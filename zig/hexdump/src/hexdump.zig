const std = @import("std");
const args = @import("zig-args");
const ioctl = @cImport({
    @cInclude("sys/ioctl.h");
});

// TODO:
//  terminal colors

const winsize = packed struct {
    ws_row: c_ushort = 0,
    ws_col: c_ushort = 0,
    ws_xpixel: c_ushort = 0,
    ws_ypixel: c_ushort = 0,
};

pub fn getTerminalSize(fd: std.c.fd_t) std.os.TermiosGetError!winsize {
    var size = winsize{};
    var res = ioctl.ioctl(fd, ioctl.TIOCGWINSZ, &size);
    switch (std.c.getErrno(res)) {
        .SUCCESS => return size,
        .NOTTY => return std.os.TermiosGetError.NotATerminal,
        else => |err| return std.os.unexpectedErrno(err),
    }
}

pub fn hexdump(file: std.fs.File, out: std.fs.File.Writer, len: usize, al: *std.mem.Allocator) !void {
    var buf_reader = std.io.bufferedReader(file.reader());
    var in = buf_reader.reader();

    var buf = try al.alloc(u8, len);
    defer al.free(buf);

    var pos: usize = 0;
    var num_read = try in.read(buf);
    while (0 < num_read) : (num_read = try in.read(buf)) {
        // Print position.
        try out.print("{x:0>8} | ", .{pos});

        // Print bytes as hex.
        for (buf[0..num_read]) |char| {
            try out.print("{x:0>2} ", .{char});
        }
        // Pad it out.
        try out.writeByteNTimes(' ', (len - num_read) * 3);

        try out.writeAll("| ");

        // Print bytes as ISO-8859-1.
        for (buf[0..num_read]) |char| {
            if (std.ascii.isPrint(char)) {
                try out.writeByte(char);
            } else {
                try out.writeAll("⋅");
            }
        }

        try out.writeByte('\n');

        pos += num_read;
    }
}

pub fn main() !void {
    var gp = std.heap.GeneralPurposeAllocator(.{ .safety = true }){};
    defer _ = gp.deinit();
    const al = &gp.allocator();

    const options = args.parseForCurrentProcess(struct {
        width: usize = 0,
        help: bool = false,
    }, al.*, .print) catch std.process.exit(1);
    defer options.deinit();

    const out = std.io.getStdOut().writer();

    if (options.options.help) {
        try out.print("Usage: {s} [ --help ] [ --width N ] [ - | FILENAME ]...\n", .{options.executable_name});
        std.process.exit(0);
    }

    const len: usize = if (options.options.width > 0) options.options.width else if (getTerminalSize(1)) |tsz|
        // As many as will fit, but a multiple of 4.
        @maximum((tsz.ws_col - 14) / 16 * 4, 4)
    else |_|
        16;

    if (options.positionals.len == 0) {
        // No args: assume stdin.
        try hexdump(std.io.getStdIn(), out, len, al);
    } else {
        for (options.positionals) |arg| {
            if (std.mem.eql(u8, arg, "-")) {
                // An arg "-" means stdin.
                try hexdump(std.io.getStdIn(), out, len, al);
            } else {
                // Other args are filenames.
                const file = std.fs.cwd().openFile(arg, .{}) catch |err| {
                    std.debug.print("input file: {s}: {s}\n", .{ arg, @errorName(err) });
                    std.process.exit(1);
                };
                defer file.close();
                try hexdump(file, out, len, al);
            }
        }
    }
}
